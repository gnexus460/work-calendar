<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class SiteController extends Controller
{
    protected $APP_ID = 'local.5c48236ed6e984.97252203';
    protected $APP_SECRET_CODE = '5exsYNl3iLmAxLyFPwHiRVp3TnW2AvUHoEWWTWZ4qy4OiXcynl';
    protected $APP_REG_URL = 'wwind.bitrix24.ua';

    const BITRIX_COUNT = 50;

    protected function executeHTTPRequest ($queryUrl, array $params = array()) {
        $result = array();
        $queryData = http_build_query($params);

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_POST => 1,
            CURLOPT_HEADER => 0,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $queryUrl,
            CURLOPT_POSTFIELDS => $queryData,
        ));

        $curlResult = curl_exec($curl);
        curl_close($curl);

        if ($curlResult != '') $result = json_decode($curlResult, true);

        return $result;
    }

    protected function requestAccessToken ($code) {
        $url = 'https://wwind.bitrix24.ua/oauth/token/?' .
            'grant_type=authorization_code'.
            '&client_id='.urlencode($this->APP_ID).
            '&client_secret='.urlencode($this->APP_SECRET_CODE).
            '&code='.urlencode($code);
        return $this->executeHTTPRequest($url);
    }

    protected function executeREST ($rest_url, $method, $params, $access_token) {
        $url = $rest_url.$method.'.json';
        return $this->executeHTTPRequest($url, array_merge($params, array("auth" => $access_token)));
    }


    protected function getDataFromApi()
    {
        $url = 'https://wwind.bitrix24.ua/rest/1140/ekcwzofbir8tv2no/user.get/';
        $header [] = "Accept: text/html, application/xml;q=0.9, application/xhtml+xml, image/png, image/jpeg, image/gif, image/x-xbitmap, */*;q=0.1";
        $header [] = "Accept-Language: ru-RU,ru;q=0.9,en;q=0.8";
        $header [] = "Accept-Charset:  utf-8, *;q=0.1";
        $header [] = "Accept-Encoding: deflate, identity, *;q=0";

        $html = [];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FAILONERROR, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        //curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header );
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_TIMEOUT_MS, 5000);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        $data = curl_exec($ch);
        curl_close($ch);

        $firstData = json_decode($data, true);

        $setCount = (int) ceil($firstData['total'] / self::BITRIX_COUNT);

        for ($i = 0; $i < $setCount; $i++) {
            $url = 'https://wwind.bitrix24.ua/rest/1140/ekcwzofbir8tv2no/user.get/?start='.(int) self::BITRIX_COUNT * $i.'';
            $header [] = "Accept: text/html, application/xml;q=0.9, application/xhtml+xml, image/png, image/jpeg, image/gif, image/x-xbitmap, */*;q=0.1";
            $header [] = "Accept-Language: ru-RU,ru;q=0.9,en;q=0.8";
            $header [] = "Accept-Charset:  utf-8, *;q=0.1";
            $header [] = "Accept-Encoding: deflate, identity, *;q=0";

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_FAILONERROR, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            //curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header );
            curl_setopt($ch, CURLOPT_TIMEOUT, 5);
            curl_setopt($ch, CURLOPT_TIMEOUT_MS, 5000);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
            $html[] = curl_exec($ch);
            curl_close($ch);
        }

        return $html;
    }



    protected function updateBxUsers()
    {

        foreach ($this->getDataFromApi() as $data) {
            $user = json_decode($data, true);
            foreach ($user['result'] as $val) {
                if ($val['EMAIL'] == '') {
                    break;
                }else {
                    User::create([
                        'id' => $val['ID'],
                        'name' => $val['NAME'],
                        'last_name' => $val['LAST_NAME'],
                        'email' => $val['EMAIL'],
                        'password' => bcrypt($val['PERSONAL_BIRTHDAY']),
                        'incoming_date' => $val['ID'],
                        'work_position' => $val['WORK_POSITION'],
                        'logo' => $val['PERSONAL_PHOTO'],
                        'is_active' => $val['ACTIVE'],
                        'is_admin' => '0'
                    ]);
                }
            }
        }
    }

}