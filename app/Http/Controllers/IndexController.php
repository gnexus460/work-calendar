<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\DB;
use App\Model\BxUser;
use App\Model\UserPriority;
use App\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class IndexController extends SiteController
{

    protected $bxUser;

    public function __construct(BxUser $bxUser)
    {
        $this->bxUser = $bxUser;
    }

    public function index(Request $request)
    {

        $myVar = $request->instance()->query('myVar');

        $bxResult = $this->executeREST($myVar['client_endpoint'], 'user.current', array(), $myVar['access_token']);

        //$this->updateBxUsers();


        $userdata = array(
            'email'     => ''.$bxResult["result"]["EMAIL"].'',
            'password'  => ''.$bxResult["result"]["PERSONAL_BIRTHDAY"].'',
        );

        if (Auth::attempt($userdata)) {

            return redirect()->route('welcome');

        }

        //return view('pages.welcome');
    }

    public function userUpdate()
    {
        $this->updateBxUsers();
        Session::flash('success','Пользователи успешно синхронизированны с Битрикс24 ');
        return redirect()->route('welcome');
    }

}
