<?php

namespace App\Http\Middleware;

use Closure;

class CheckBitrixAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */


    public function handle($request, Closure $next)
    {
        if(!isset($_REQUEST['code'])) {
            return redirect("https://wwind.bitrix24.ua/oauth/authorize/?response_type=code&client_id=$this->APP_ID&redirect_uri=https://vacation.wwind.ua/");
        } elseif (!isset($_REQUEST['access_token'])) {
            $arAccessParams = $this->requestAccessToken($_REQUEST['code']);
            $request->merge(array("myVar" => $arAccessParams));
            return $next($request);
        } else {
            return $next($request);
        }
//        $step = 0;
//        if (!isset($_REQUEST['code'])) $step = 1;
//        if (isset($_REQUEST['code'])) $step = 2;
//        switch ($step) {
//
//            case 1:
//                return redirect("https://ssd.bitrix24.ua/oauth/authorize/?response_type=code&client_id=$this->APP_ID&redirect_uri=https://magento.wwind.ua/");
////                $testReq = $this->executeHTTPRequest("https://ssd.bitrix24.ua/oauth/authorize/?response_type=code&client_id=$this->APP_ID&redirect_uri=https://magento.wwind.ua/");
//                break;
//
//            case 2:
//
//                $arAccessParams = $this->requestAccessToken($_REQUEST['code']);
//                $request->merge(array("myVar" => $arAccessParams));
////                return redirect()->back();
//                return $next($request);
//
//                break;
//            default:
//                break;
//        }
    }

    protected $APP_ID = 'local.5c48236ed6e984.97252203';
    protected $APP_SECRET_CODE = '5exsYNl3iLmAxLyFPwHiRVp3TnW2AvUHoEWWTWZ4qy4OiXcynl';
    protected $APP_REG_URL = 'http://vacation.wwind.ua/';

    protected function executeHTTPRequest ($queryUrl, array $params = array()) {
        $result = array();
        $queryData = http_build_query($params);

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_POST => 1,
            CURLOPT_HEADER => 0,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $queryUrl,
            CURLOPT_POSTFIELDS => $queryData,
        ));

        $curlResult = curl_exec($curl);
        curl_close($curl);

        if ($curlResult != '') $result = json_decode($curlResult, true);

        return $result;
    }

    protected function requestAccessToken ($code) {
        $url = 'https://wwind.bitrix24.ua/oauth/token/?' .
            'grant_type=authorization_code'.
            '&client_id='.urlencode($this->APP_ID).
            '&client_secret='.urlencode($this->APP_SECRET_CODE).
            '&code='.urlencode($code);
        return $this->executeHTTPRequest($url);
    }

    protected function executeREST ($rest_url, $method, $params, $access_token) {
        $url = $rest_url.$method.'.json';
        return $this->executeHTTPRequest($url, array_merge($params, array("auth" => $access_token)));
    }

}
