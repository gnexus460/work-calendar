<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::post('/', function () {
//    return view('welcome');
//});


//Route::get('/', function (){
//    return view('welcome');
//})->name('test');

//Route::match(['get', 'post'],'/', 'IndexController@index')->name('index');

Route::match(['get', 'post'],'/', 'IndexController@index')->name('index');
Route::match(['get', 'post'],'/welcome', 'PagesController@index')->name('welcome')->middleware('checkBitrixAuth');
Route::resource('calendar', 'CalendarController');
//Route::get('calendar/create ', 'CalendarController@create')->name('calendar.create ')->middleware('auth');

//Route::group(['middleware' => ['web']], function(){
//    Route::get('calendar/create ', 'CalendarController@create')->name('calendar.create ')->middleware('auth');
//    Route::get('calendar/show ', 'CalendarController@show')->name('calendar.show ');
//});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
