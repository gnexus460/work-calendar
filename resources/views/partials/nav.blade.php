<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="nav-icon">
        <img src="{{ asset('img/640px-React-icon.svg.png') }}" alt="">
    </div>
    <a class="navbar-brand" href="{{ route('index') }}">Планирование отпуска</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse main-nav" id="navbarNavDropdown">
        <ul class="navbar-nav">
            @if (Auth::guest())
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">Login</a>
                </li>
            @else
                <li class="nav-item p-2">
                    <a class="btn btn-outline-primary" href="{{route('user.update')}}">Обновление пользователей</a>
                </li>
                <li class="nav-item p-2">
                    <a class="btn btn-outline-primary" href="{{route('calendar.create')}}">Создать календарь</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{ Auth::user()->name }}
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item"
                           href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                           document.getElementById('logout-form').submit();"
                        >
                                Logout
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </div>
                </li>
            @endif
                {{--<li class="nav-item active">--}}
                    {{--<a class="btn btn-primary" href="{{ route("welcome") }}">--}}
                        {{--welcome--}}
                    {{--</a>--}}
                {{--</li>--}}
        </ul>
    </div>
</nav>