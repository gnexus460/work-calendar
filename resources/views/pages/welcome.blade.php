@extends('main')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <div class="list-group" id="list-tab" role="tablist">
                    <a class="list-group-item list-group-item-action active" id="list-home-list" href="{{ route('calendar.show', 1) }}" role="tab" aria-controls="home">Лето 2019</a>
                    <a class="list-group-item list-group-item-action" id="list-profile-list" href="{{ route('calendar.show', 2) }}" role="tab" aria-controls="profile">Зима 2018-2019</a>
                    <a class="list-group-item list-group-item-action" id="list-messages-list" href="{{ route('calendar.show', 3) }}" role="tab" aria-controls="messages">Лето 2018</a>
                    <a class="list-group-item list-group-item-action" id="list-settings-list" href="{{ route('calendar.show', 4) }}" role="tab" aria-controls="settings">Зима 2017-2018</a>
                </div>
            </div>
        </div>
    </div>
@endsection

