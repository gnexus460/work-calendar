@extends('main')
@section('style')
    <link href="{{ asset('react/build') }}/static/css/1.047f16fc.chunk.css" rel="stylesheet">
    <link href="{{ asset('react/build') }}/static/css/main.38ce6ab0.chunk.css" rel="stylesheet">
@endsection
@section('content')
    <div class="container">
        <div class="date-pick-container">
            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <div class="row">
                        <div id="root"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="users-list">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Имя</th>
                    <th scope="col">Фамилия</th>
                    <th scope="col">Статус отпуска</th>
                    <th scope="col">Приоритет</th>
                    <th>Зависимость</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th scope="row">1</th>
                    <td>Mark</td>
                    <td>Otto</td>
                    <td>
                        <label class="switch">
                            <input type="checkbox" checked>
                            <span class="slider round"></span>
                        </label>
                    </td>
                    <td>
                        <label class="switch">
                            <input type="checkbox">
                            <span class="slider round"></span>
                        </label>
                    </td>
                    <th>
                        <select class="custom-select">
                            <option selected>Open this select menu</option>
                            <option value="1">One</option>
                            <option value="2">Two</option>
                            <option value="3">Three</option>
                        </select>
                    </th>
                </tr>
                <tr class="table-active">
                    <th scope="row">2</th>
                    <td>Jacob</td>
                    <td>Thornton</td>
                    <td>
                        <label class="switch">
                            <input type="checkbox" checked>
                            <span class="slider round"></span>
                        </label>
                    </td>
                    <td>
                        <label class="switch">
                            <input type="checkbox">
                            <span class="slider round"></span>
                        </label>
                    </td>
                    <th>
                        <select class="custom-select">
                            <option selected>Open this select menu</option>
                            <option value="1">One</option>
                            <option value="2">Two</option>
                            <option value="3">Three</option>
                        </select>
                    </th>
                </tr>
                <tr>
                    <th scope="row">3</th>
                    <td>Larry</td>
                    <td>the Bird</td>
                    <td>
                        <label class="switch">
                            <input type="checkbox" checked>
                            <span class="slider round"></span>
                        </label>
                    </td>
                    <td>
                        <label class="switch">
                            <input type="checkbox">
                            <span class="slider round"></span>
                        </label>
                    </td>
                    <th>
                        <select class="custom-select">
                            <option selected>Open this select menu</option>
                            <option value="1">One</option>
                            <option value="2">Two</option>
                            <option value="3">Three</option>
                        </select>
                    </th>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection
@section('script')
    <script>
        ! function(l) {
            function e(e) {
                for (var r, t, n = e[0], o = e[1], u = e[2], f = 0, i = []; f < n.length; f++) t = n[f], p[t] && i.push(p[t][0]), p[t] = 0;
                for (r in o) Object.prototype.hasOwnProperty.call(o, r) && (l[r] = o[r]);
                for (s && s(e); i.length;) i.shift()();
                return c.push.apply(c, u || []), a()
            }

            function a() {
                for (var e, r = 0; r < c.length; r++) {
                    for (var t = c[r], n = !0, o = 1; o < t.length; o++) {
                        var u = t[o];
                        0 !== p[u] && (n = !1)
                    }
                    n && (c.splice(r--, 1), e = f(f.s = t[0]))
                }
                return e
            }
            var t = {},
                p = {
                    2: 0
                },
                c = [];

            function f(e) {
                if (t[e]) return t[e].exports;
                var r = t[e] = {
                    i: e,
                    l: !1,
                    exports: {}
                };
                return l[e].call(r.exports, r, r.exports, f), r.l = !0, r.exports
            }
            f.m = l, f.c = t, f.d = function(e, r, t) {
                f.o(e, r) || Object.defineProperty(e, r, {
                    enumerable: !0,
                    get: t
                })
            }, f.r = function(e) {
                "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(e, Symbol.toStringTag, {
                    value: "Module"
                }), Object.defineProperty(e, "__esModule", {
                    value: !0
                })
            }, f.t = function(r, e) {
                if (1 & e && (r = f(r)), 8 & e) return r;
                if (4 & e && "object" == typeof r && r && r.__esModule) return r;
                var t = Object.create(null);
                if (f.r(t), Object.defineProperty(t, "default", {
                    enumerable: !0,
                    value: r
                }), 2 & e && "string" != typeof r)
                    for (var n in r) f.d(t, n, function(e) {
                        return r[e]
                    }.bind(null, n));
                return t
            }, f.n = function(e) {
                var r = e && e.__esModule ? function() {
                    return e.default
                } : function() {
                    return e
                };
                return f.d(r, "a", r), r
            }, f.o = function(e, r) {
                return Object.prototype.hasOwnProperty.call(e, r)
            }, f.p = "/";
            var r = window.webpackJsonp = window.webpackJsonp || [],
                n = r.push.bind(r);
            r.push = e, r = r.slice();
            for (var o = 0; o < r.length; o++) e(r[o]);
            var s = n;
            a()
        }([])
    </script>
    <script src="{{ asset('react/build') }}/static/js/1.ab2779d4.chunk.js"></script>
    <script src="{{ asset('react/build') }}/static/js/main.699fbd15.chunk.js"></script>
@endsection

