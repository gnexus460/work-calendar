import React from "react";
import dateFns from "date-fns";

class BlockDatesCell extends React.Component {
    state = {
      currentMonth: this.props.props,
      blockedWeeks: []
    };

    pickBlockWeek (day) {
      const dateFormat = "DD/MM/YYYY";
      const blockedWeeks = this.state.blockedWeeks;
      const pickedWeek = dateFns.format(day,dateFormat);

      if(blockedWeeks.indexOf(pickedWeek) === -1) {
        this.setState({
            blockedWeeks: [...this.state.blockedWeeks, pickedWeek]
        });
      } else {
        this.setState({
          blockedWeeks: this.state.blockedWeeks.filter((el) => {
            return el !== pickedWeek;
          })
        });
      }
      console.log(blockedWeeks)
    }

    renderWeeks() {
        const dateFormat = "DD";
        const dateFormated = "DD/MM/YYYY";

        const blockedWeeks = this.state.blockedWeeks;
        let monthStart = this.props.props,
            monthEnd = dateFns.endOfMonth(monthStart),
            mondaysFormated = [];
        while(monthStart < monthEnd)
        {

            // Выбираем понедельники
            if(dateFns.isMonday(monthStart))
            {
              const curMonth = monthStart;
              console.log();

                mondaysFormated.push(
                    <div onClick={() => {this.pickBlockWeek(curMonth)}}
                         className={`week-cell ${blockedWeeks.indexOf(dateFns.format(curMonth,dateFormated)) >= 0 ? 'selected': ''}`}
                         key={dateFns.format(monthStart,dateFormat)}


                    >
                        {dateFns.format(monthStart,dateFormat)}
                    </div>


                );
            }
            monthStart = dateFns.addDays(monthStart, 1);
        }
        return <div className="mondays-container">
        <input id="prodId" name={`blockedWeeks[${blockedWeeks}]`} type="hidden" />
        {mondaysFormated}
        </div>;
    }

    render () {
      return (
        <div className="block-weeks-container">
          {this.renderWeeks()}
        </div>
      )
    }
}

export default BlockDatesCell;
