import React from "react";
import dateFns from "date-fns";

class CalendarInput extends React.Component {
    state = {
        currentYear: new Date(),
        selectedMonth: '',
        showMenu: false,
        inputValue: this.props.placeholder
    };

    renderHeader() {
        return (
            <div className="header row flex-middle ">
                <div className="col-md-2 col-start">
                    <div className="icon" onClick={this.prevYear}>
                        chevron_left
                    </div>
                </div>
                <div className="col-md-8 col-center">
                    {/*<span>{dateFns.formatmonth-cell selected(this.state.currentMonth, dateFormat)}</span>*/}
                    <span>{dateFns.getYear(this.state.currentYear)}</span>
                </div>
                <div className="col-md-2 col-end" onClick={this.nextYear}>
                    <div className="icon">chevron_right</div>
                </div>
            </div>
        );
    }

    nextYear = () => {
        this.setState({
            currentYear: dateFns.addYears(this.state.currentYear, 1)
        });
    };

    prevYear = () => {
        this.setState({
            currentYear: dateFns.subYears(this.state.currentYear, 1)
        });
    };

    pickMonth = day => {
        this.setState({
            selectedMonth: day,
            inputValue: `${day.toLocaleString('ru', { month: 'long' })} ${dateFns.getYear(day)}`,
        });
    };



    renderMonths() {
        var startYear = dateFns.startOfYear(this.state.currentYear),
            months = [];
        const selectedMonth = this.state.selectedMonth;
        for (var i = 0; i < 12; i++) {
            const nextMonth = dateFns.addMonths(startYear, i);
            months.push(
                <div onClick={ () => {
                    this.pickMonth(nextMonth);
                    this.toggleMonthForm();
                    this.props.updateData(
                        nextMonth
                    );
                }}
                     className={`month-cell ${dateFns.isSameDay(nextMonth, selectedMonth) ? "selected":''}`}
                     key={i}>
                    {nextMonth.toLocaleString('ru', { month: 'short' })}
                </div>
            );
        }
        return <div className="months-container">{months}</div>;
    }

    toggleMonthForm = () => {
        this.setState({
            showMenu: !this.state.showMenu
        })
    };

    render() {
        const menuVis = this.state.showMenu ? 'active' : 'hide';

        return (
            <div className="form-group col-md-6">
                <input onClick={this.toggleMonthForm} type="email" readOnly={true} className={`form-control date-pick-input ${menuVis}`} id="exampleInputEmail1"
                       aria-describedby="emailHelp" value={this.state.inputValue}/>
                <div className={`month-pick-form ${menuVis}`}>
                    {this.renderHeader()}
                    {this.renderMonths()}
                </div>
            </div>
        )
    }
}

export default CalendarInput;
