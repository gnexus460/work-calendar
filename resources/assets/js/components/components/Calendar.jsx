
import React from "react";
import dateFns from "date-fns";
import CalendarInput from "./CalendarInput";
import BlockDatesCell from "./BlockDatesCell"

class Calendar extends React.Component {
    state = {
        vacationStart: null,
        vacationEnd: null,
        currentMonthHeader: null,
        selectedDate: new Date(),
        blockedWeeks: []
    };

    renderWeeksHeader() {
        let currentMonthHeader = this.state.currentMonthHeader,
            vacationStart = this.state.vacationStart,
            vacationEnd = this.state.vacationEnd;
;
        if(vacationStart && vacationEnd && dateFns.isBefore(vacationStart,vacationEnd)) {
            return(
                <div className="header row flex-middle ">
                    <div className="col-md-2 col-start">
                        <div className="icon" onClick={this.prevMonth}>
                            chevron_left
                        </div>
                    </div>
                    <div className="col-md-8 col-center">
                        <span>{`${currentMonthHeader.toLocaleString('ru', { month: 'long' })} ${dateFns.getYear(currentMonthHeader)}`}</span>
                    </div>
                    <div className="col-md-2 col-end" onClick={this.nextMonth}>
                        <div className="icon">chevron_right</div>
                    </div>
                    <BlockDatesCell props={currentMonthHeader} />
                </div>
            );
        } else {
           return(
               <div className="alert alert-danger">
                   Выберите период
               </div>
           )
        }
    }

    prevMonth = () => {
        const currentMonthHeader = this.state.currentMonthHeader;
        const vacationStart = this.state.vacationStart;
        if(dateFns.isAfter(currentMonthHeader, vacationStart)) {
            this.setState({
                currentMonthHeader: dateFns.subMonths(this.state.currentMonthHeader, 1)
            });
        }
    };

    nextMonth = () => {
        const currentMonthHeader = this.state.currentMonthHeader;
        const vacationEnd = this.state.vacationEnd;
        if(dateFns.isBefore(currentMonthHeader, vacationEnd)) {
            this.setState({
                currentMonthHeader: dateFns.addMonths(this.state.currentMonthHeader, 1)
            });
        }
    };

    renderBlockWeeks() {
        const menuVis = this.state.showMenu ? 'active' : 'hide';

        return (
            <div className="col-md-12">
                <input onClick={this.toggleBlockWeeks}
                type="email"
                readOnly={true}
                className={`form-control date-pick-input ${menuVis}`}
                id="exampleInputEmail1"
                aria-describedby="emailHelp"
                value="Заблокировать даты"/>

                <div className={`month-pick-form ${menuVis}`}>
                    {this.renderWeeksHeader()}
                </div>

            </div>
        );
    }

    //
    // renderDays() {
    //     const dateFormat = "dddd";
    //     const days = [];
    //
    //     let startDate = dateFns.startOfWeek(this.state.currentMonth);
    //
    //     for (let i = 0; i < 7; i++) {
    //         days.push(
    //             <div className="col col-center" key={i}>
    //                 {dateFns.format(dateFns.addDays(startDate, i), dateFormat)}
    //             </div>
    //         );
    //     }
    //
    //     return <div className="days row">{days}</div>;
    // }
    //
    // renderCells() {
    //     const { currentMonth, selectedDate } = this.state;
    //     const monthStart = dateFns.startOfMonth(currentMonth);
    //     const monthEnd = dateFns.endOfMonth(monthStart);
    //     const startDate = dateFns.startOfWeek(monthStart);
    //     const endDate = dateFns.endOfWeek(monthEnd);
    //
    //     const dateFormat = "D";
    //     const rows = [];
    //
    //     let days = [];
    //     let day = startDate;
    //     let formattedDate = "";
    //
    //     while (day <= endDate) {
    //         for (let i = 0; i < 7; i++) {
    //             formattedDate = dateFns.format(day, dateFormat);
    //             const cloneDay = day;
    //             days.push(
    //                 <div
    //                     className={`col cell ${
    //                         !dateFns.isSameMonth(day, monthStart)
    //                             ? "disabled"
    //                             : dateFns.isSameDay(day, selectedDate) ? "selected" : ""
    //                         }`}
    //                     key={day}
    //                     onClick={() => this.onDateClick(dateFns.parse(cloneDay))}
    //                 >
    //                     <span className="number">{formattedDate}</span>
    //                     <span className="bg">{formattedDate}</span>
    //                 </div>
    //             );
    //             day = dateFns.addDays(day, 1);
    //         }
    //         rows.push(
    //             <div className="row" key={day}>
    //                 {days}
    //             </div>
    //         );
    //         days = [];
    //     }
    //     return <div className="body">{rows}</div>;
    // }
    //
    // onDateClick = day => {
    //     this.setState({
    //         selectedDate: day
    //     });
    // };
    //
    // nextMonth = () => {
    //     this.setState({
    //         currentMonth: dateFns.addMonths(this.state.currentMonth, 1)
    //     });
    // };
    //
    // prevMonth = () => {
    //     this.setState({
    //         currentMonth: dateFns.subMonths(this.state.currentMonth, 1)
    //     });
    // };

    updateVacationStart = (value) => {
        this.setState({ vacationStart: value,
                        currentMonthHeader: value})
    };

    updateVacationEnd = (value) => {
        this.setState({ vacationEnd: value })
    };

    toggleBlockWeeks = () => {
        this.setState({
            showMenu: !this.state.showMenu
        })
    };

    render() {
        return (
            <div className="date-picker-container">
                <form>
                    <div className="row">
                        <CalendarInput id="vacationStart" updateData={this.updateVacationStart} placeholder="Начало сезона" />
                        <CalendarInput id="vacationEnd" updateData={this.updateVacationEnd} placeholder="Окончание сезона" />

                        {this.renderBlockWeeks()}
                    </div>
                </form>
            </div>
        );
    }
}

export default Calendar;
